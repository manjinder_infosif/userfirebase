package com.omninos.firstmobapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;

/**
 * Created by Manjinder Singh on 17 , December , 2019
 */
public class DemoAdapter extends RecyclerView.Adapter<DemoAdapter.MyViewHolder> {

    Context context;
    Choose choose;

    public DemoAdapter(Context context, Choose choose) {
        this.context = context;
        this.choose = choose;
    }

    interface Choose {
        void SetData(int data,String status);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.demo_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.buttonPanel.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                if (oldValue>newValue){
                    choose.SetData(1,"0");
                }else if (newValue>oldValue){
                    choose.SetData(1,"1");
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return 40;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ElegantNumberButton buttonPanel;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            buttonPanel = itemView.findViewById(R.id.buttonPanel);

        }
    }

//    @Override
//    public long getItemId(int position) {
//        return super.getItemId(position);
//    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}
