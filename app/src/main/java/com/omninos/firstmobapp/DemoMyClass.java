package com.omninos.firstmobapp;

import java.io.Serializable;

/**
 * Created by Manjinder Singh on 23 , December , 2019
 */
public class DemoMyClass implements Serializable {
    String name,number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
