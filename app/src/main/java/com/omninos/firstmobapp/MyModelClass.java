package com.omninos.firstmobapp;

/**
 * Created by Manjinder Singh on 27 , December , 2019
 */
public class MyModelClass {
    String name;
    String status;

    public MyModelClass(String name, String status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
