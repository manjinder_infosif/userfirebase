package com.omninos.firstmobapp.MyNavigationDemo;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.omninos.firstmobapp.MyNavigationDemo.MyViewModel.MyGitViewModel;
import com.omninos.firstmobapp.MyNavigationDemo.adapter.MyNewAdapter;
import com.omninos.firstmobapp.MyNavigationDemo.model.Project;
import com.omninos.firstmobapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {

    public static final String TAG = "TAG First";
    private MyGitViewModel viewModel;
    private RecyclerView recyclerView;
    private MyNewAdapter adapter;
    private List<Project> list = new ArrayList<>();


    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_first, container, false);

        viewModel = ViewModelProviders.of(getActivity()).get(MyGitViewModel.class);
        Log.d(TAG, "onCreateView: ");
        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);


        viewModel.GetUpdateList().observe(getActivity(), new Observer<List<Project>>() {
            @Override
            public void onChanged(List<Project> projects) {

                adapter = new MyNewAdapter(getActivity(), projects, new MyNewAdapter.Choose() {
                    @Override
                    public void Select(int position) {
                        Project project=new Project();
                        project.setName("Ankit");
                        project.setLanguage("C++");
                        projects.add(project);
                        viewModel.SetData(projects);
                        NavDirections action = FirstFragmentDirections.actionFirstFragmentToSecondFragment().setName(projects.get(position).getName());
                        Navigation.findNavController(view).navigate(action);
                    }
                });
                recyclerView.setAdapter(adapter);
            }
        });

        return view;
    }
}

