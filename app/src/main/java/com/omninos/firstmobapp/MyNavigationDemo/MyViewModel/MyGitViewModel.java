package com.omninos.firstmobapp.MyNavigationDemo.MyViewModel;

import android.app.Activity;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.omninos.firstmobapp.MyNavigationDemo.model.Project;
import com.omninos.firstmobapp.MyNavigationDemo.retrofit.Api;
import com.omninos.firstmobapp.MyNavigationDemo.retrofit.ApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Manjinder Singh on 18 , December , 2019
 */
public class MyGitViewModel extends ViewModel {

    private MutableLiveData<List<Project>> listMutableLiveData;

    private MutableLiveData<List<Project>> updateData = new MutableLiveData<>();

    public LiveData<List<Project>> listLiveData(Activity activity) {

        if (listMutableLiveData == null) {
            listMutableLiveData = new MutableLiveData<>();
            Api api = ApiClient.getApiClient().create(Api.class);
            Call<List<Project>> call = api.getList();
            call.enqueue(new Callback<List<Project>>() {
                @Override
                public void onResponse(Call<List<Project>> call, Response<List<Project>> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(activity, "Success", Toast.LENGTH_SHORT).show();
                        listMutableLiveData.setValue(response.body());
                        call.cancel();
                    }
                }
                @Override
                public void onFailure(Call<List<Project>> call, Throwable t) {
                    Toast.makeText(activity, t.toString(), Toast.LENGTH_SHORT).show();
                    call.cancel();
                }
            });
        }
        return listMutableLiveData;
    }

    public void SetData(List<Project> projects) {
        updateData.setValue(projects);
    }

    public LiveData<List<Project>> GetUpdateList() {
        return updateData;
    }
}
