package com.omninos.firstmobapp.MyNavigationDemo;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.omninos.firstmobapp.MyNavigationDemo.MyViewModel.MyGitViewModel;
import com.omninos.firstmobapp.MyNavigationDemo.model.Project;
import com.omninos.firstmobapp.R;

import java.util.List;

public class NavActivity extends AppCompatActivity {

    MyGitViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav);

        viewModel = ViewModelProviders.of(this).get(MyGitViewModel.class);

        viewModel.listLiveData(this).observe(NavActivity.this, new Observer<List<Project>>() {
            @Override
            public void onChanged(List<Project> projects) {
                viewModel.SetData(projects);
            }
        });

        System.gc();
    }
}
