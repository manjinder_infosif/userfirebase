package com.omninos.firstmobapp.MyNavigationDemo;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.omninos.firstmobapp.MyNavigationDemo.MyViewModel.MyGitViewModel;
import com.omninos.firstmobapp.MyNavigationDemo.model.Project;
import com.omninos.firstmobapp.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {
    public static final String TAG = "TAG Second";
    private TextView text;
    private MyGitViewModel viewModel;

    public SecondFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_second, container, false);

        viewModel = ViewModelProviders.of(getActivity()).get(MyGitViewModel.class);
        Log.d(TAG, "onCreateView: ");
        text = view.findViewById(R.id.text);
        text.setText(SecondFragmentArgs.fromBundle(getArguments()).getName());

        viewModel.GetUpdateList().observe(getActivity(), new Observer<List<Project>>() {
            @Override
            public void onChanged(List<Project> projects) {
                for (int i = 0; i < projects.size(); i++) {
                    System.out.println("Data: " + projects.get(i).getName());
                }
            }
        });
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requireActivity().onBackPressed();
            }
        });
        return view;
    }

}
