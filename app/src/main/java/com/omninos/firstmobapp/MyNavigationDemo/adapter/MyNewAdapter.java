package com.omninos.firstmobapp.MyNavigationDemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.omninos.firstmobapp.MyNavigationDemo.model.Project;
import com.omninos.firstmobapp.R;

import java.util.List;

/**
 * Created by Manjinder Singh on 18 , December , 2019
 */
public class MyNewAdapter extends RecyclerView.Adapter<MyNewAdapter.MyViewHolder> {

    Context context;
    private List<Project> list;
    Choose choose;




    public interface Choose {
        void Select(int position);
    }

    public MyNewAdapter(Context context, List<Project> list, Choose choose) {
        this.context = context;
        this.list = list;
        this.choose = choose;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.title.setText(list.get(position).getName());
        holder.language.setText(list.get(position).getLanguage());
        holder.mainFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choose.Select(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title, language;
        private LinearLayout mainFile;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            language = itemView.findViewById(R.id.language);
            mainFile=itemView.findViewById(R.id.mainFile);
        }
    }
}
