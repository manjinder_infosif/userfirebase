package com.omninos.firstmobapp.MyNavigationDemo.retrofit;

import com.omninos.firstmobapp.MyNavigationDemo.model.Project;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Manjinder Singh on 18 , December , 2019
 */
public interface Api {
    @GET("users/manjhi/repos")
    Call<List<Project>> getList();
}
