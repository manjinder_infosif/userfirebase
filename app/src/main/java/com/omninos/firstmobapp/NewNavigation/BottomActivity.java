package com.omninos.firstmobapp.NewNavigation;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.omninos.firstmobapp.R;

public class BottomActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom);

        if (savedInstanceState == null) {
            loadData(R.navigation.first_nav);
        }

        navigation = findViewById(R.id.bottom);


        navigation.setOnNavigationItemSelectedListener(this);
    }

    private void loadData(int first_nav) {
        NavHostFragment finalHost = NavHostFragment.create(first_nav);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.nav_host_fragment1, finalHost)
                .setPrimaryNavigationFragment(finalHost) // this is the equivalent to app:defaultNavHost="true"
                .commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.home:
                Toast.makeText(this, "first", Toast.LENGTH_SHORT).show();
                loadData(R.navigation.first_nav);

                break;

            case R.id.home1:
                Toast.makeText(this, "second", Toast.LENGTH_SHORT).show();
                loadData(R.navigation.second_nav);

                break;

            case R.id.home2:
                Toast.makeText(this, "third", Toast.LENGTH_SHORT).show();
                loadData(R.navigation.third_nav);

                break;
        }
        return true;
    }

}
