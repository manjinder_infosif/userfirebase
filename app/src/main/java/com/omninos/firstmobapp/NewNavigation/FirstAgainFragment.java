package com.omninos.firstmobapp.NewNavigation;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.krishna.fileloader.FileLoader;
import com.krishna.fileloader.listener.FileRequestListener;
import com.krishna.fileloader.pojo.FileResponse;
import com.krishna.fileloader.request.FileLoadRequest;
import com.omninos.firstmobapp.R;

import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirstAgainFragment extends Fragment {

    PDFView pdfView;

    public FirstAgainFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_first_again, container, false);

        pdfView=view.findViewById(R.id.pdf_viewer);
        FileLoader.with(getActivity()).load("http://oshc.zizsoft.com/oshc_testing.pdf")
//                .fromDirectory("PDFFiles",FileLoader.DIR_EXTERNAL_PUBLIC)
                .asFile(new FileRequestListener<File>() {
                    @Override
                    public void onLoad(FileLoadRequest request, FileResponse<File> response) {
                        pdfView.fromFile(response.getBody())
                                .enableDoubletap(true)
                                .onPageChange(new OnPageChangeListener() {
                                    @Override
                                    public void onPageChanged(int page, int pageCount) {
                                        Toast.makeText(getActivity(), ""+page+": "+pageCount, Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .load();
                    }

                    @Override
                    public void onError(FileLoadRequest request, Throwable t) {

                    }
                });
      return view;
    }
}
