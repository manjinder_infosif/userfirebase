package com.omninos.firstmobapp.NewNavigation;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.omninos.firstmobapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirstNavFragment extends Fragment {
    DemoCollectionPagerAdapter demoCollectionPagerAdapter;
    ViewPager viewPager;


    public FirstNavFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_first_nav, container, false);
//        TextView move = view.findViewById(R.id.move);

        demoCollectionPagerAdapter = new DemoCollectionPagerAdapter(getChildFragmentManager());
        viewPager = view.findViewById(R.id.pager);
        viewPager.setAdapter(demoCollectionPagerAdapter);

//        move.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                NavDirections action = FirstNavFragmentDirections.actionFirstNavFragmentToFirstAgainFragment();
//                Navigation.findNavController(view).navigate(action);
//            }
//        });

        return view;
    }


    private class DemoCollectionPagerAdapter extends FragmentStatePagerAdapter {

        public DemoCollectionPagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Tab1Fragment();

                case 1:
                    return new Tab2Fragment();

            }
            return new Tab1Fragment();
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
