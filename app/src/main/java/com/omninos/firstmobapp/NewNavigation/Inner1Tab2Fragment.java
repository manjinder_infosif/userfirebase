package com.omninos.firstmobapp.NewNavigation;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.omninos.firstmobapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Inner1Tab2Fragment extends Fragment {


    public Inner1Tab2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inner1_tab2, container, false);
    }

}
