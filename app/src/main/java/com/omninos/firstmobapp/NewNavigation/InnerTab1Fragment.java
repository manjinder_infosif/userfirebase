package com.omninos.firstmobapp.NewNavigation;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.omninos.firstmobapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InnerTab1Fragment extends Fragment {


    public InnerTab1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_inner_tab1, container, false);

        Button buttonPanel=view.findViewById(R.id.buttonPanel);
        buttonPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavDirections action=InnerTab1FragmentDirections.actionInnerTab1FragmentToInner1Tab1Fragment();
                Navigation.findNavController(view).navigate(action);
            }
        });

        return view;
    }

}
