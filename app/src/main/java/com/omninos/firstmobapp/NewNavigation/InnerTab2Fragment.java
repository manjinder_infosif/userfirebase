package com.omninos.firstmobapp.NewNavigation;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.omninos.firstmobapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InnerTab2Fragment extends Fragment {


    public InnerTab2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inner_tab2, container, false);

        Button tab2Button = view.findViewById(R.id.tab2Button);
        tab2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavDirections action = InnerTab2FragmentDirections.actionInnerTab2FragmentToInner1Tab2Fragment();
                Navigation.findNavController(view).navigate(action);
            }
        });
        return view;
    }

}
