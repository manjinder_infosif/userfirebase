package com.omninos.firstmobapp.NewNavigation;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.omninos.firstmobapp.MyModelClass;
import com.omninos.firstmobapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecondNavFragment extends Fragment {

    TextView textClear;
    RecyclerView recyclerView;

    private List<MyModelClass> list = new ArrayList<>();
    MyNewAdapter adapter;

    int val = 0;


    public SecondNavFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_second_nav, container, false);
        textClear = view.findViewById(R.id.textClear);
        recyclerView = view.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        MyModelClass myModelClass = new MyModelClass("Car", "0");
        list.add(myModelClass);
        myModelClass = new MyModelClass("Bike", "0");
        list.add(myModelClass);
        myModelClass = new MyModelClass("Bus", "0");
        list.add(myModelClass);
        myModelClass = new MyModelClass("Truck", "0");
        list.add(myModelClass);

//        list.add("Car");
//        list.add("Bike");
//        list.add("Bus");
//        list.add("Truck");
        adapter = new MyNewAdapter(getActivity(), list, new MyNewAdapter.CheckData() {
            @Override
            public void Selcet(String status, int position) {
                list.get(position).setStatus(status);
                adapter.notifyDataSetChanged();
            }
        });
        recyclerView.setAdapter(adapter);

        textClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.unselectall();
            }
        });
        return view;
    }

//    public void onCheckboxClicked(View view) {
//        boolean checked = ((CheckBox) view).isChecked();
//
//        switch (view.getId()) {
//            case R.id.check1:
//
//                break;
//
//            case R.id.check2:
//
//                break;
//
//            case R.id.check3:
//
//                break;
//
//            case R.id.check4:
//
//                break;
//        }
//    }


    public static class MyNewAdapter extends RecyclerView.Adapter<MyNewAdapter.MyViewHolder> {


        Context context;
        List<MyModelClass> data;
        CheckData checkData;
        boolean isSelectedAll = false;

        public MyNewAdapter(Context context, List<MyModelClass> data, CheckData checkData) {
            this.context = context;
            this.data = data;
            this.checkData = checkData;
        }

        public interface CheckData {
            void Selcet(String status, int postion);
        }


        public void selectAll() {
            isSelectedAll = true;
            notifyDataSetChanged();
        }

        public void unselectall() {
            isSelectedAll = false;
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public MyNewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.custom_item_check, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyNewAdapter.MyViewHolder holder, int position) {
            holder.checkBox1.setText(data.get(position).getName());
            if (!isSelectedAll) {
                holder.checkBox1.setChecked(false);
            } else {
                holder.checkBox1.setChecked(true);
            }
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            CheckBox checkBox1;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                checkBox1 = itemView.findViewById(R.id.check1);
            }
        }
    }
}
