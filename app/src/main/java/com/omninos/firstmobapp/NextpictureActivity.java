package com.omninos.firstmobapp;

import android.app.PictureInPictureParams;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Rational;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

public class NextpictureActivity extends AppCompatActivity {

    private TextView vv;
    //    private Toolbar tb;
//    private Button pip;
//    private Uri videoUri;\
    VideoView videoView;
    private PictureInPictureParams.Builder pictureInPictureParamsBuilder;


    private String defaultVideo =
            "http://mirrors.standaloneinstaller.com/video-sample/metaxas-keller-Bell.mp4";


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        startPictureInPictureFeature();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nextpicture);
        vv = findViewById(R.id.vv);
        videoView = findViewById(R.id.videoView);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            pictureInPictureParamsBuilder = new PictureInPictureParams.Builder();
        } else {
            Toast.makeText(this, "Not Support", Toast.LENGTH_SHORT).show();
        }

        final MediaController mediacontroller = new MediaController(this);
        mediacontroller.setAnchorView(videoView);
//        String videoUrl = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4";
        String videoUrl = "android.resource://" + getPackageName() + "/" + R.raw.sample_video;
        videoView.setMediaController(mediacontroller);
        videoView.setVideoURI(Uri.parse(videoUrl));
        videoView.requestFocus();
        videoView.start();


//        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
//                    @Override
//                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
//                        videoView.setMediaController(mediacontroller);
//                        mediacontroller.setAnchorView(videoView);
//
//                    }
//                });
//            }
//        });

        vv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPictureInPictureFeature();
            }
        });
    }

    @Override
    public void onUserLeaveHint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!isInPictureInPictureMode()) {
                Rational aspectRatio = new Rational(videoView.getWidth(), videoView.getHeight());
                pictureInPictureParamsBuilder.setAspectRatio(aspectRatio).build();
                enterPictureInPictureMode(pictureInPictureParamsBuilder.build());
            }
        } else {
            Toast.makeText(this, "Not Support", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPictureInPictureModeChanged(boolean isInPictureInPictureMode,
                                              Configuration newConfig) {
        if (isInPictureInPictureMode) {

        } else {
        }
    }

    private void startPictureInPictureFeature() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Rational aspectRatio = new Rational(videoView.getWidth(), videoView.getHeight());
            pictureInPictureParamsBuilder.setAspectRatio(aspectRatio).build();
            enterPictureInPictureMode(pictureInPictureParamsBuilder.build());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        videoView.resume();
    }
}
