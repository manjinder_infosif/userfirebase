package com.omninos.firstmobapp;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TestActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private DemoAdapter adapter;
    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        initView();
        SetUp();

    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerView);
        text = findViewById(R.id.text);
        text.setText("0");
    }

    private void SetUp() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);


        adapter = new DemoAdapter(TestActivity.this, new DemoAdapter.Choose() {
            @Override
            public void SetData(int data,String status) {
                int lastdata = Integer.parseInt(text.getText().toString());
                if (status.equalsIgnoreCase("0")){
                    int newData = lastdata - data;
                    text.setText("" + newData);
                }else {
                    int newData = lastdata + data;
                    text.setText("" + newData);
                }


            }
        });
        recyclerView.setAdapter(adapter);

    }
}
